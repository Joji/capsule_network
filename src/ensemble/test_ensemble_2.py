import os
import sys
from datetime import datetime
from time import perf_counter
import numpy as np

import torch
import torch.nn as nn
from torch.optim import Adam

batch_size = 1

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
download_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../data")
model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_1")
model_path2 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_2")
model_path3 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_3")
model_path4 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_4")
model_path5 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_5")
model_path6 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_6")
model_path7 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../models/model_ensemble_7")

from cifar_loader import cifar_loader
from cifar_ensemble_model import CifarNet

train_loader, test_loader = cifar_loader(batch_size, download_path=download_path)

model = CifarNet()
model2 = CifarNet()
model3 = CifarNet()
model4 = CifarNet()
model5 = CifarNet()
model6 = CifarNet()
model7 = CifarNet()
model.load_state_dict(torch.load(model_path, map_location="cpu"))
model.eval() # Set evaluation mode
model2.load_state_dict(torch.load(model_path2, map_location="cpu"))
model2.eval() # Set evaluation mode
model3.load_state_dict(torch.load(model_path3, map_location="cpu"))
model3.eval() # Set evaluation mode
model4.load_state_dict(torch.load(model_path4, map_location="cpu"))
model4.eval() # Set evaluation mode
model5.load_state_dict(torch.load(model_path5, map_location="cpu"))
model5.eval() # Set evaluation mode
model6.load_state_dict(torch.load(model_path6, map_location="cpu"))
model6.eval() # Set evaluation mode
model7.load_state_dict(torch.load(model_path7, map_location="cpu"))
model7.eval() # Set evaluation mode
test_loss = 0
correct = 0.
total_count = 0.
n_test_batches = len(test_loader)
for batch_id, (data, target) in enumerate(test_loader):
    tar = target
    target = torch.sparse.torch.eye(10).index_select(dim=0, index=target)
    # if self.config["gpu"]:
    #     data, target = data.cuda(), target.cuda()

    output, reconstructions, masked = model(data)
    output2, _, masked2 = model2(data)
    output3,_,masked3 = model3(data)
    output4,_,masked4 = model4(data)
    output5,_,masked5 = model5(data)
    output6,_,masked6 = model6(data)
    output7,_,masked7 = model7(data)
    vec = [ 0 for i in range(10)]

    vec[torch.argmax(masked.data,1)] += 1
    vec[torch.argmax(masked2.data,1)] += 1
    vec[torch.argmax(masked3.data,1)] += 1
    vec[torch.argmax(masked4.data,1)] += 1
    vec[torch.argmax(masked5.data,1)] += 1
    vec[torch.argmax(masked6.data,1)] += 1
    vec[torch.argmax(masked7.data,1)] += 1
    vec = np.array(vec)
    result = np.argmax(vec)
    # Shape of classes: (batch_size, num_classes)
    # _, max_indices = classes.max(dim=1)
    # print(max_indices, tar)
    # if self.config["gpu"]:
    #     loss = model.module.loss(data, output, target, reconstructions)
    # else:
    # loss = model.loss(data, output, target, reconstructions)

    # test_loss += loss.data.item()
    correct += torch.sum(result == tar).cpu().numpy().squeeze()
    total_count += len(data)
    accuracy = correct/total_count
    if (batch_id % 1 == 0):
        print("Testing - Batch: {}/{}, Accuracy: {}".format( batch_id+1, n_test_batches, accuracy))
