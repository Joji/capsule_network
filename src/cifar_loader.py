import torch
import torchvision
import torchvision.transforms as transforms

def cifar_loader(batch_size, download_path="data", transform=transforms.ToTensor(), target_transform=None, shuffle=False):
    """Returns a training dataset loader and a test dataset loader.

    Args:
        batch_size (int): The number of images per batch
        download_path (string): The path of CIFAR dataset
        transform: Tranform to be applied on input
        target_transform: Transform to be applied on target
        shuffle: Indicates shuffle the dataset or not

    Example:
        >>> test_loader, train_loader = cifar_loader(100, "./data")
    """
    train_dataset = torchvision.datasets.CIFAR10(download_path, train=True, download=True, transform=transform, target_transform=target_transform)
    test_dataset = torchvision.datasets.CIFAR10(download_path, train=False, download=True, transform=transform, target_transform=target_transform)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size, shuffle=shuffle)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size, shuffle=shuffle)
    return train_loader, test_loader

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    train_loader, test_loader = cifar_loader(100)
    plt.imshow(train_loader.dataset[1][0].permute(1,2,0).detach().numpy())
    plt.show()
