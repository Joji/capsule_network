class AnimalsTransform(object):
    """Convert a CIFAR 10 target category to animal category.

    Converts a CIFAR 10 target category to animal category. New categories are
    as follows
    0 - Bird
    1 - Cat
    2 - Deer
    3 - Dog
    4 - Frog
    5 - Horse
    6 - None of the above
    """

    def __call__(self, target):
        """
        Args:
            target (int): CIFAR10 category.

        Returns:
            out_target (int): Converted target.
        """
        if 1 < target < 8:
            out_target = target - 2
        else:
            out_target = 6
        return out_target

    def __repr__(self):
        return self.__class__.__name__ + '()'
