import os
import sys
from datetime import datetime
from time import perf_counter

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../capsule"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

import torch
import torch.nn as nn
from torch.optim import Adam

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-G", "--gpu", help="Use gpu for training", action="store_true")
parser.add_argument("-E", "--epochs", help="Number of epochs", type=int, default=100)
parser.add_argument("-B", "--batch_size", help="Batch size", type=int, default=100)
args = parser.parse_args()
# Do it before loading anything
USE_CUDA = False
if args.gpu:
    USE_CUDA = True
    os.environ["USE_CUDA"] = str(USE_CUDA)

from cifar_loader import cifar_loader
from animals import Animals
from animals_transform import AnimalsTransform
from trainer import Trainer

# Changing current working directory to the directory of source code
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

# Creating an instance of CapsNet
caps_net = Animals()

# if os.path.isfile('model_animals'):
#     print("model_animals exists")
#     caps_net.load_state_dict(torch.load('model_animals', map_location="cpu"))
# else:
#     print("model_animals doesn't exists")

if USE_CUDA:
    caps_net = caps_net.cuda()
    caps_net = nn.DataParallel(caps_net)


optimizer = Adam(caps_net.parameters())
download_path = os.path.join(os.path.dirname(__file__), "../data/")
# Creating an instance of CapsNet
# cifar_net = CifarNet()
# if USE_CUDA:
#     cifar_net = cifar_net.cuda()
#     cifar_net = nn.DataParallel(cifar_net)

n_epochs = args.epochs
batch_size = args.batch_size
config = {"model_name":"model_animals", "epochs":n_epochs, "batch_size":args.batch_size, "gpu":USE_CUDA, "num_classes":7}
train_loader, test_loader = cifar_loader(batch_size, download_path=download_path, target_transform=AnimalsTransform())
# cifar_net.load_state_dict(torch.load("model_cifar_3_3_3_m", map_location=torch.device('cpu')))
trainer = Trainer(caps_net, train_loader, test_loader, config)
trainer.train()
