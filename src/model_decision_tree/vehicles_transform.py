class VehiclesTransform(object):
    """Convert a CIFAR 10 target category to vehicles category.

    Converts a CIFAR 10 target category to vehicles category. New categories are
    as follows
    0 - Airplane
    1 - Automobile
    2 - Ship
    3 - Truck
    4 - None of the above
    """

    def __call__(self, target):
        """
        Args:
            target (int): CIFAR10 category.

        Returns:
            out_target (int): Converted target.
        """
        out_target = target
        if 1 < target <= 7:
            out_target = 4
        elif 8 <= target < 10:
            out_target = out_target-5
        return out_target

    def __repr__(self):
        return self.__class__.__name__ + '()'
