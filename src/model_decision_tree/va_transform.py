class VaTransform(object):
    """Convert a CIFAR 10 target category to vehicle/animal.

    Converts a CIFAR 10 target category to vehicles/animals. New category 0
    corresponds to vehicle and 1 corresponds to animal
    """

    def __call__(self, target):
        """
        Args:
            target (int): CIFAR10 category.

        Returns:
            out_target (int): Converted target.
        """
        out_target = 0 if target in [0,1,8,9] else 1
        return out_target

    def __repr__(self):
        return self.__class__.__name__ + '()'
