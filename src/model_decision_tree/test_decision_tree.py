import torch
import sys, os
from tqdm import tqdm
from time import sleep
import torch.nn as nn
from animals import Animals
from vehicles_animals import VehiclesAnimals
from vehicles import Vehicles

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../capsule"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from cifar_loader import cifar_loader

from va_transform import VaTransform
from animals_transform import AnimalsTransform
from vehicles_transform import VehiclesTransform
from cifar_net import CifarNet


va = VehiclesAnimals()
animals = Animals()
vehicles = Vehicles()
cifar_net = CifarNet()

# va = nn.DataParallel(va)
# animals = nn.DataParallel(animals)
# vehicles = nn.DataParallel(vehicles)
# t = torch.load('model_va', map_location=torch.device('cpu'))
# print(t)
model_va = os.path.join(os.path.dirname(__file__), "../../models/model_va")
model_vehicles = os.path.join(os.path.dirname(__file__), "../../models/model_vehicles")
model_animals = os.path.join(os.path.dirname(__file__), "../../models/model_animals")
model_cifar = os.path.join(os.path.dirname(__file__), "../../src/model_cifar")
va.load_state_dict(torch.load(model_va, map_location=torch.device('cpu')))
vehicles.load_state_dict(torch.load(model_vehicles, map_location=torch.device('cpu')))
animals.load_state_dict(torch.load(model_animals,map_location=torch.device('cpu')))
cifar_net.load_state_dict(torch.load(model_cifar,map_location=torch.device('cpu')))
#
download_path = os.path.join(os.path.dirname(__file__), "../data/")

train_loader, test_loader = cifar_loader(1, download_path=download_path)
#
correct = 0.
for idx, (data, target) in (enumerate(test_loader)):#, total = len(test_loader)):
    # print(data)
    root_data = data.clone()
    # root_data[0] = VaTransform()(data[0].data.item())
    output, reconstructions, masked = va(data)
    root_prediction = torch.argmax(masked.data, 1)
    if root_prediction == VaTransform()(target[0]):
        if root_prediction == 0:
            vehicle_data = data.clone()
            # vehicle_data[0] = VehiclesTransform()(data[0])
            _,_,masked = vehicles(vehicle_data)
            vehicle_prediction = torch.argmax(masked.data, 1)
            if vehicle_prediction == VehiclesTransform()(target[0]):
                correct+=1
            if vehicle_prediction == 4:
                _,_,masked = cifar_net(data)
                if (torch.argmax(masked.data,1) == target[0]):
                    correct+=1
        if root_prediction == 1:
            animal_data = data.clone()
            # vehicle_data[0] = VehiclesTransform()(data[0])
            _,_,masked = vehicles(animal_data)
            animal_prediction = torch.argmax(masked.data, 1)
            if animal_prediction == AnimalsTransform()(target[0]):
                correct+=1
            if animal_prediction == 6:
                _,_,masked = cifar_net(data)
                if (torch.argmax(masked.data,1) == target[0]):
                    correct+=1
        # if root_prediction == 1:
    print(correct/(idx+1.))

    # print(torch.argmax(masked.data, 1))
