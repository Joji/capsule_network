import os
import sys
from datetime import datetime
from time import perf_counter

import torch
import torch.nn as nn
from torch.optim import Adam

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), "../capsule"))
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-G", "--gpu", help="Use gpu for training", action="store_true")
parser.add_argument("-E", "--epochs", help="Number of epochs", type=int, default=100)
parser.add_argument("-B", "--batch_size", help="Batch size", type=int, default=100)
args = parser.parse_args()
# Do it before loading anything
USE_CUDA = False
if args.gpu:
    USE_CUDA = True
    os.environ["USE_CUDA"] = str(USE_CUDA)

from cifar_loader import cifar_loader
from cifar_5_3_net import CifarNet

# Changing current working directory to the directory of source code
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

# Creating an instance of CifarNet
cifar_net = CifarNet()
if USE_CUDA:
    cifar_net = cifar_net.cuda()
    cifar_net = nn.DataParallel(cifar_net)

# cifar_net.load_state_dict(torch.load('model'))
optimizer = Adam(cifar_net.parameters())

n_epochs = args.epochs
batch_size = args.batch_size
train_loader, test_loader = cifar_loader(batch_size, download_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "../data/"))
accuracy = 0
t_start = perf_counter()
n_test_batches, n_train_batches = len(test_loader), len(train_loader)
print("Starting training of CIFAR-10 5-3 model")
print("Using GPU") if args.gpu else print("Using CPU")
start_date_time = datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
print("Start Time:\t\t{} (GPU Server Time)".format(start_date_time))
print("Number of epochs:\t{}".format(args.epochs))
print("Batch size:\t\t{}\n".format(args.batch_size))
for epoch in range(n_epochs):
    print("Starting epoch {}".format(epoch+1))
    print("------------------")
    cifar_net.train() # Set training mode
    train_loss = 0
    correct = 0.
    total_count = 0.
    for batch_id, (data, target) in enumerate(train_loader):
        target = torch.sparse.torch.eye(10).index_select(dim=0, index=target) # Make target a 10 element vector

        if USE_CUDA:
            data, target = data.cuda(), target.cuda()

        optimizer.zero_grad()
        output, reconstructions, masked = cifar_net(data)
        loss = cifar_net.loss(data, output, target, reconstructions)
        loss.backward()
        optimizer.step()

        train_loss += loss.data.item()
        correct += torch.sum(torch.argmax(masked.data, 1) == torch.argmax(target.data, 1)).cpu().numpy().squeeze()
        total_count += len(data)
        if (batch_id % 10 == 0):
            print("Training - Epoch: {}/{}, Batch: {}/{}, Loss: {}, Accuracy: {}".format(epoch+1, n_epochs, batch_id+1, n_train_batches, train_loss, correct/total_count))
    print("Total loss = {}, accuracy = {}".format(train_loss, correct/total_count))


    torch.save(cifar_net.state_dict(), "model_cifar_5_3")

    cifar_net.eval() # Set evaluation mode
    test_loss = 0
    correct = 0.
    total_count = 0.
    for batch_id, (data, target) in enumerate(test_loader):
        target = torch.sparse.torch.eye(10).index_select(dim=0, index=target)
        if USE_CUDA:
            data, target = data.cuda(), target.cuda()

        output, reconstructions, masked = cifar_net(data)
        loss = cifar_net.loss(data, output, target, reconstructions)

        test_loss += loss.data.item()
        correct += torch.sum(torch.argmax(masked.data, 1) == torch.argmax(target.data, 1)).cpu().numpy().squeeze()
        total_count += len(data)
        accuracy = correct/total_count
        if (batch_id % 10 == 0):
            print("Testing - Epoch: {}/{}, Batch: {}/{}, Loss: {}, Accuracy: {}".format(epoch+1, n_epochs, batch_id+1, n_test_batches, test_loss, accuracy))
    print("Total test loss = {}, accuracy = {}".format(test_loss, accuracy))
    print("")
t_end = perf_counter()

print("Summary")
print("=======")
print("Number of epochs:\t{}".format(n_epochs))
print("Batch size:\t\t{}".format(batch_size))
print("Validation Accuracy:\t{}%".format(accuracy*100))
print("Start Time:\t\t{} (GPU Server Time)".format(start_date_time))
print("End Time:\t\t{} (GPU Server Time)".format(datetime.now().strftime("%Y-%m-%d, %H:%M:%S")))
print("Time elapsed:\t\t{} seconds".format(t_end - t_start))
