import os
import sys
from datetime import datetime
from time import perf_counter

import torch
import torch.nn as nn
from torch.optim import Adam

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
download_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../data")

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-G", "--gpu", help="Use gpu for training", action="store_true")
parser.add_argument("-E", "--epochs", help="Number of epochs", type=int, default=100)
parser.add_argument("-B", "--batch_size", help="Batch size", type=int, default=100)
args = parser.parse_args()
# Do it before loading anything
USE_CUDA = False
if args.gpu:
    USE_CUDA = True
    os.environ["USE_CUDA"] = str(USE_CUDA)

from cifar_loader import cifar_loader
from cifar_3_3_3_m_net import CifarNet

from trainer import Trainer

# Creating an instance of CapsNet
cifar_net = CifarNet()
if USE_CUDA:
    cifar_net = cifar_net.cuda()
    cifar_net = nn.DataParallel(cifar_net)

n_epochs = args.epochs
batch_size = args.batch_size
config = {"model_name":"model_cifar_3_3_3_m", "epochs":n_epochs, "batch_size":args.batch_size, "gpu":USE_CUDA}
train_loader, test_loader = cifar_loader(batch_size, download_path=download_path)
# cifar_net.load_state_dict(torch.load("model_cifar_3_3_3_m", map_location=torch.device('cpu')))
trainer = Trainer(cifar_net, train_loader, test_loader, config)
trainer.train()
