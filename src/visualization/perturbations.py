import os, sys
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))

from mnist_loader import mnist_loader
from caps_net import CapsNet
# Changing current working directory to the directory of source code
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

caps_net = CapsNet()
model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../models/model_mnist')
data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../data/')
caps_net.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))

_, test_loader = mnist_loader(10, download_path=data_path)

data = torch.stack([test_loader.dataset[8][0]], dim=0);

output, reconstructions, masked = caps_net(data)
digit = torch.argmax(torch.sum(output**2, dim=2), dim=1)
img = []
for dim in range(16):
    val = -0.25
    row = []
    while val <= 0.25:
        modified_out = output.clone()
        modified_out[0][digit.data.item()][dim] = val
        decoder_output,_ = caps_net.decoder(modified_out)
        row.append(decoder_output.view(28,28))
        val = val + 0.05
    row = torch.cat(row, dim=1)
    img.append(row)
img = torch.cat(img, dim=0)
plt.imshow(img.detach().numpy(), cmap="gray")
plt.axis("off")
plt.show()
