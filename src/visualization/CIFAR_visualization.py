import os,sys
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),".."))

from cifar_loader import cifar_loader
from cifar_net import CifarNet

cifar_net = CifarNet()
model_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../models/model_cifar')
data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../data')
cifar_net.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))

_, test_loader = cifar_loader(100, download_path=data_path)


data = torch.stack([test_loader.dataset[i][0] for i in range(50)], dim=0)
_, reconstructions,_ = cifar_net(data)
in_img = []
out_img = []
for r in range(5):
    in_row = []
    out_row = []
    for c in range(10):
        in_row.append(data[r*10+c].permute(1,2,0))
        out_row.append(reconstructions[r*10+c].permute(1,2,0))
    in_row = torch.cat(in_row, dim=1)
    in_img.append(in_row)
    out_row = torch.cat(out_row, dim=1)
    out_img.append(out_row)
in_img = torch.cat(in_img)
out_img = torch.cat(out_img)
visualization = torch.cat([in_img, out_img]).detach().numpy()

plt.imshow(visualization)
plt.axis('off')
plt.show()
