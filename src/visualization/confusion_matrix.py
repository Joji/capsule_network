import torch
import numpy as np
from torch.optim import Adam
import torchvision
import torchvision.transforms as transforms
import torchvision.transforms.functional as F
from matplotlib import pyplot as plt

import os, sys
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
from cifar_net import CifarNet

cifar_model = CifarNet()
model_path = os.path.join(os.path.dirname(__file__), '../model_cifar')
data_path = os.path.join(os.path.dirname(__file__), '../data/')
cifar_model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
#import helper
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
os.chdir('..')

download_path="data"

test_dataset = torchvision.datasets.CIFAR10(download_path , train=False, download=True, transform=transforms.ToTensor())
test_loader = torch.utils.data.DataLoader(test_dataset, 1)
classes = ['plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

test_images = []
test_images_label = []

for image, label in test_loader:
	test_images.append(image)
	test_images_label.append(label)


predicted = np.zeros(10000)
labels = np.zeros(10000)



#cifar10_model.load_state_dict(torch.load(os.path.join(dname, '../src/model_cifar'), map_location=torch.device('cpu')))
cifar_model.eval()

for i in range(0,10000):
	predict = torch.stack([test_images[i][0]], dim = 0)
	out, _ , _ = cifar_model(predict)
	out = out.squeeze(-1)
	out = out ** 2
	out = out.sum(dim=2)
	out = out.view(10)
	prediction = np.argmax(out.data.numpy().squeeze())
	predicted[i] = int(prediction)
	labels[i] = (test_images_label[i][0])
	if i%500 == 0:
	    print("No.of images processed = {}".format(i))

predicted_tensor = torch.from_numpy(predicted)
label_tensor = torch.from_numpy(labels)

stacked = torch.stack(
    (
        label_tensor
        ,predicted_tensor
    ) 
    , dim = 1
)
cmt = torch.zeros(10,10, dtype=torch.int64)
for p in stacked:
    tl, pl = p.tolist()
    cmt[int(tl), int(pl)] = cmt[int(tl), int(pl)] +  1

print(cmt)


import itertools

def plot_confusion_matrix(cm, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    #plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

os.chdir(dname)

plt.figure(figsize=(10,10))
plot_confusion_matrix(cmt, classes)
plt.savefig('confusion_matrix_cifar.jpg')
