#!/bin/bash

#SBATCH --job-name=cifar10
#SBATCH --output=out_cifar10.txt
#SBATCH --gres=gpu:1
#SBATCH --partition=cl1_12h-2G

srun python ../src/train_cifar.py -G -E 20
