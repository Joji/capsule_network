#!/bin/bash

#SBATCH --job-name=cifar10
#SBATCH --output=out_train_cifar_3_3_3.txt
#SBATCH --gres=gpu:4
#SBATCH --partition=cl1_all_4G

srun python ../src/small_kernel_model/train_cifar_3_3_3.py -G -E 20 -B 50
