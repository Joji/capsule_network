#!/bin/bash

#SBATCH --job-name=VA
#SBATCH --output=out_VA.txt
#SBATCH --gres=gpu:4
#SBATCH --partition=cl1_all_4G

srun python ../src/model_decision_tree/train_va.py -G -B 100 -E 20
