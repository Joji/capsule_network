#!/bin/bash

#SBATCH --job-name=cifar10
#SBATCH --output=out_E3.txt
#SBATCH --gres=gpu:4
#SBATCH --partition=cl1_all_4G

srun python ../src/ensemble/train_ensemble.py -G -B 100 -L 3 -E 20
