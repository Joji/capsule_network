#!/bin/bash

#SBATCH --job-name=vehicles
#SBATCH --output=out_vehicles.txt
#SBATCH --gres=gpu:4
#SBATCH --partition=cl1_all_4G

srun python ../src/model_decision_tree/train_vehicles.py -G -B 100 -E 20
