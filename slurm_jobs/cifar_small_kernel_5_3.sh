#!/bin/bash

#SBATCH --job-name=cifar10
#SBATCH --output=out_train_cifar_5_3.txt
#SBATCH --gres=gpu:2
#SBATCH --partition=cl1_12h-2G

srun python ../src/small_kernel_model/train_cifar_5_3.py -G -E 20
