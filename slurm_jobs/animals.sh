#!/bin/bash

#SBATCH --job-name=animals
#SBATCH --output=out_animals.txt
#SBATCH --gres=gpu:4
#SBATCH --partition=cl1_all_4G

srun python ../src/model_decision_tree/train_animals.py -G -B 100 -E 20
