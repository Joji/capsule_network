#!/bin/bash

#SBATCH --job-name=mnist
#SBATCH --output=out_mnist.txt
#SBATCH --gres=gpu:1
#SBATCH --partition=cl1_12h-2G

srun python ../src/train.py -G -E 50
