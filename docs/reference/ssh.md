# SSH - Secure Shell

## Establish a connection

**Connect to vpn** (if needed)

Type the following in terminal

```bash
ssh username@ip-address
```

It will ask for password. After providing the password we can use the remote machine using terminal.

## Exit from SSH

Type the following in terminal

```bash
exit
```

## Copy a file from local machine to remote machine

```bash
scp localfile username@ip-address:/path/to/dest
```

Example

```bash
scp hello.txt user@10.10.10.10:/home/user/temp/
```

Then provide the password.

This command copies `hello.txt` from local machine to `/home/user/temp` folder of remote server.

## Copy a file from remote machine to local machine

```bash
scp username@ip-address:/path/to/file /path/to/dest/
```

Example

```bash
scp user@10.10.10.10:/home/user/temp/hello.txt ~/Desktop/
```

Then provide the password.

This command copies `hello.txt` from remote server to `Desktop` folder of local machine.
