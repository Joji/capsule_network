# Git

**In case any doubt refer [git-scm.com](https://git-scm.com/)**

## Clone a repository

```bash
git clone https://gitlab.com/Joji/capsule_network.git
```

## List the branches

```bash
git branch
```

## Create new branch

```bash
git branch new_branch
```

## Change branch

```bash
git checkout new_branch
```

## Shorthand for creating and changing to new branch

```bash
git checkout -b new_branch
```

## Show the status of files in the repository

```bash
git status
```

## Show the difference between previous version (commit) and current version

```bash
git diff
```

## Prepare files to be commited (In other words prepare files to be stored in history)

```bash
git add filename
```

## Commit the changes (In other words save the changes)

```bash
git commit -m "Meaningful commit message"
```

## Push the changes to remote repo

```bash
git push origin branch_name
```

## Fetch some data from remote branch

```bash
git fetch origin remote_branch
```

## Merge another branch to current branch

```bash
git merge another_branch
```

## Pull the changes from remote_branch (fetch + merge)

```bash
git pull origin branch_name
```
