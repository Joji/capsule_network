# Capsule Network

## Directory Structure

Folder/File      | Description
---------------- | ----------------------------------------------------------------------------------------------
docs             | Contains report, presentation and other documents
playground       | For experimentation
playground_local | For experimentation. But it is gitignored so that the folders will not get updated in the repo
src              | We write our source code here
src/capsule      | Contains source code of capsule layers
server           | To show demonstration

## First steps

Create and activate our virtual environment

```bash
python3 -m venv env
source env/bin/activate # Linux
env\Scripts\activate.bat # Windows
```

Please activate the virtual environment everytime you open a new shell.

Install the required packages

```bash
pip install -r requirements.txt
```

Update `requirements.txt` after installing new packages

```bash
pip freeze > requirements.txt
```

## Linter

Always use `pylint /path/to/python/code` before pushing the changes. Type,

```bash
make lint
```

To lint files in src folder.

## Run server

```bash
make run_server
```

## Train in GPU

ssh into your GPU account.

Clone the repo, install packages, change virtual environment and change into desired branch.

Go to `slurm_jobs` folder

Type following command

```bash
sbatch test.sh
```
